import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/shareReplay';
import {JwtHelperService} from '@auth0/angular-jwt';
import {User} from '../../models/user';
import {environment} from '../../../environments/environment';

@Injectable()
export class AuthService {
  loginUrl = environment.serverUrl + '/users/login';
  logoutUrl = environment.serverUrl + '/users/me/token';
  registerUrl = environment.serverUrl + '/users';

  user: User = JSON.parse(localStorage.getItem('user'));

  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) {
  }

  login(email: string, password: string) {
    return this.http.post<any>(this.loginUrl, {email, password}, {observe: 'response'})
      .shareReplay();
  }

  logout() {
    return this.http.delete<any>(this.logoutUrl);
  }

  loggedIn() {
    return (localStorage.getItem('x-auth') !== null) && !this.jwtHelper.isTokenExpired();
  }

  register(user: User) {
    return this.http.post<any>(this.registerUrl, user, {observe: 'response'})
      .shareReplay();
  }

}
