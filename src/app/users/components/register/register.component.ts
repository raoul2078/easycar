import {AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {User} from '../../../models/user';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit, OnDestroy, AfterViewInit {
  user: User = {
    firstName: '',
    lastName: '',
    gender: null,
    email: '',
    password: '',
    role: 'member',
    birth: '',
    address: {
      street: '',
      city: '',
      zip: '',
      country: ''
    },
    phoneNumber: '',
  };

  model;

  minDate = {year: 1920, month: 1, day: 1};

  constructor(private authService: AuthService,
              private router: Router,
              private flashMsgService: FlashMessagesService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    document.querySelector('body').classList.add('body');
    document.querySelector('nav').classList.add('navbar-transparent');
  }

  ngOnDestroy() {
    document.querySelector('body').classList.add('body');
    document.querySelector('nav').classList.remove('navbar-transparent');
  }

  setGender(valueGender){
    console.log(valueGender);
    this.user.gender = valueGender;
  }

  register({value, valid}: { value: User, valid: boolean }) {
    this.user.birth = this.model.year + '-' + this.model.month + '-' + this.model.day;
    this.authService.register(this.user)
      .subscribe((res) => {
          this.flashMsgService.show('Votre compte a été créé', {
            cssClass: 'alert alert-success', timeout: 4000
          });
          localStorage.setItem('x-auth', res.headers.get('x-auth'));
          localStorage.setItem('user', JSON.stringify(res.body));
          this.authService.user = res.body;
          this.router.navigate(['/dashboard']);
        },
        (err) => {
          console.log(err);
          let errmsg = '';
          if (err.error.errmsg !== undefined) {
            if (err.error.errmsg.indexOf('duplicate key')) {
              errmsg = 'Cette adresse email est déjà utilisée';
            }
          } else {
            errmsg = err.error.message;
          }
          this.flashMsgService.show(errmsg, {
            cssClass: 'alert alert-danger', timeout: 4000
          });
          this.router.navigate(['/register']);
        });
  }
}
