import { Component, OnInit } from '@angular/core';
import {FlashMessagesService} from 'angular2-flash-messages';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {User} from '../../../models/user';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  user: User;
  current = '';
  newPassword = '';
  confirm = '';

  constructor(private authService: AuthService,
              private userService: UserService,
              private router: Router,
              private flashMsgService: FlashMessagesService) {
    this.user = this.authService.user;
  }

  ngOnInit() {
  }


  change({value, valid}: { value: any, valid: boolean }) {
    console.log(value);
    if (valid) {
      this.user.password = this.newPassword;
      this.userService.changePassword(value).subscribe(
        (res) => {
          this.flashMsgService.show('Modifications sauvegardées', {
            cssClass: 'alert alert-success', timeout: 4000
          });
          localStorage.removeItem('x-auth');
          localStorage.removeItem('user');
          this.router.navigate(['/login']);
        },
        (err) => {
          console.log(err);

          this.flashMsgService.show(err.error, {
            cssClass: 'alert alert-danger', timeout: 4000
          });
        }
      );
    }
  }

}
