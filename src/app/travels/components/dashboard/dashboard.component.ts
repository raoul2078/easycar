import {AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthService} from '../../../users/services/auth.service';
import {TravelType} from '../../../models/travelType';
import {TravelService} from '../../services/travel.service';
import {Travel} from '../../../models/travel';
import {UserService} from '../../../users/services/user.service';
import {Car} from '../../../models/car';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit, OnDestroy, AfterViewInit {

  p1 = 1;
  p2 = 1;
  p3 = 1;
  p4 = 1;
  allTravels: Travel[];
  travels: Travel[];
  travelsAsPassenger: Travel[];
  travelsHistoryAsPassenger: Travel[];
  revenues = 0;
  nbTravels = 0;
  nbUsers = 0;
  nbCars = 0;

  constructor(public authService: AuthService,
              private travelService: TravelService,
              private userService: UserService) {
    this.travelService.getTravels().subscribe(travels => {
      this.travels = travels;
    });

    this.travelService.getTravelsAsPassenger().subscribe(travels => {
      this.travelsAsPassenger = travels;
    });

    this.travelService.getTravelsHistoryAsPassenger().subscribe(travels => {
      this.travelsHistoryAsPassenger = travels;
    });

    this.travelService.getAllTravels().subscribe(travels => {
      this.allTravels = travels;
      this.nbTravels = travels.length;
      for (let i = 0; i < travels.length; i++) {
        this.revenues += travels[i].price * (travels[i].passengers === undefined ? 0 : travels[i].passengers.length);
      }

      this.userService.getUsers().subscribe(users => {
        this.nbUsers = users.length;
      });
    });

    this.travelService.getCars().subscribe(cars => {
      this.nbCars = cars.length;
    });

  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    document.querySelector('nav').classList.add('dashboard-nav');
    document.querySelector('body').classList.remove('body');
  }

  ngOnDestroy() {
    document.querySelector('nav').classList.remove('dashboard-nav');
  }

}
