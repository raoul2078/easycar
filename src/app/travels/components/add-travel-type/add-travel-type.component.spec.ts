import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddTravelTypeComponent} from './add-travel-type.component';

describe('AddTravelTypeComponent', () => {
  let component: AddTravelTypeComponent;
  let fixture: ComponentFixture<AddTravelTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddTravelTypeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTravelTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
