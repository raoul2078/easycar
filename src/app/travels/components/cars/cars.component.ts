import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Car} from '../../../models/car';
import {TravelService} from '../../services/travel.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CarsComponent implements OnInit {
  cars: Car[];
  p = 1;

  constructor(private carService: TravelService,
              private flashMsgService: FlashMessagesService,
              private router: Router) {
    
    this.carService.getCars().subscribe(cars => {
      this.cars = cars;
    });
  }

  ngOnInit() {
  }

  deleteCar(id: string) {
    
    this.carService.deleteCar(id).subscribe(
      (res) => {
        this.flashMsgService.show('voiture supprimé', {
          cssClass: 'alert alert-success', timeout: 4000
        });
        this.router.navigate(['/cars']);
      },
      (err) => {
        this.flashMsgService.show(err.error.message, {
          cssClass: 'alert alert-danger', timeout: 4000
        });
        this.router.navigate(['/cars']);
      }
    );
  }


}
