import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NgxPaginationModule} from 'ngx-pagination';
import {Travel} from '../../../models/travel';
import {TravelService} from '../../services/travel.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';

@Component({
  selector: 'app-travels',
  templateUrl: './travels.component.html',
  styleUrls: ['./travels.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TravelsComponent implements OnInit {
  travels: Travel[];
  Math: any;
  p = 1;

  constructor(private travelService: TravelService,
              private flashMsgService: FlashMessagesService,
              private router: Router) {
    this.Math = Math;
    
    this.travelService.getTravels().subscribe(travels => {
      this.travels = travels;
    });
  }

  key = 'name'; // set default
  reverse = false;

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  ngOnInit() {
  }

  deleteTravel(id: string) {
    
    this.travelService.deleteTravel(id).subscribe(
      (res) => {
        this.flashMsgService.show('Trajet supprimé', {
          cssClass: 'alert alert-success', timeout: 4000
        });
        this.router.navigate(['/dashboard']);
      },
      (err) => {
        this.flashMsgService.show(err.error.message, {
          cssClass: 'alert alert-danger', timeout: 4000
        });
        this.router.navigate(['/dashboard']);
      }
    );
  }

  undeleteTravel(id: string) {
    
    this.travelService.undeleteTravel(id).subscribe(
        (res) => {
          this.flashMsgService.show('Trajet supprimé', {
            cssClass: 'alert alert-success', timeout: 4000
          });
          this.router.navigate(['/dashboard']);
        },
        (err) => {
          this.flashMsgService.show(err.error.message, {
            cssClass: 'alert alert-danger', timeout: 4000
          });
          this.router.navigate(['/dashboard']);
        }
    );
  }

}
