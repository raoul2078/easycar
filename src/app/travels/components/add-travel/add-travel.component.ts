import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';
import {TravelService} from '../../services/travel.service';
import {Travel} from '../../../models/travel';
import {Car} from '../../../models/car';
import {TravelType} from '../../../models/travelType';
import * as $ from 'jquery';
import {ModalDismissReasons, NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

declare let google;

@Component({
  selector: 'app-add-travel',
  templateUrl: './add-travel.component.html',
  styleUrls: ['./add-travel.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddTravelComponent implements OnInit {
  @ViewChild('content') content: ElementRef;
  private modalRef: NgbModalRef;
  closeResult: string;

  model;
  minDate = {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate()};
  directionsService = new google.maps.DirectionsService;

  time = {hour: 13, minute: 30};

  travel: Travel = {
    departure: '',
    destination: '',
    departureAddress: '',
    destinationAddress: '',
    averageTime: 0,
    distance: 0,
    price: 0,
    date: new Date,
    placesAvailable: 0,
    car: {},
  };

  cars: Car[];
  travelTypes: TravelType[] = [];

  constructor(private travelService: TravelService,
              private router: Router,
              private flashMsgService: FlashMessagesService,
              private modalService: NgbModal) {

    this.travelService.getCars().subscribe(cars => {
      this.cars = cars;
    });
  }

  ngOnInit() {
  }

  open(content) {
    this.modalRef = this.modalService.open(content);

    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  setprice() {

    console.log((<HTMLInputElement>document.getElementById('distance')).value);

    this.travelService.getTravelTypeDistance(Number((<HTMLInputElement>document.getElementById('distance')).value))
      .subscribe(travelType => {
        this.travelTypes = travelType;
      });
    if (this.travelTypes.length === 0) {
      (<HTMLInputElement>document.getElementById('price')).max =
        (Number((<HTMLInputElement>document.getElementById('distance')).value) * 0.5).toFixed(2);
    }
    if (this.travelTypes.length !== 0) {
      (<HTMLInputElement>document.getElementById('price')).max =
        (Number((<HTMLInputElement>document.getElementById('distance')).value) * this.travelTypes[0].price).toFixed(2);
    }
    this.travelTypes = [];
  }

  autoCompleteCallback1(selectedData: any) {
    this.travelTypes = [];
    console.log(selectedData.data.vicinity);
    console.log(selectedData.data.formatted_address);
    const result = selectedData.data.vicinity;
    const substring = ',';
    let getid;
    if (result.indexOf(substring) >= 0) {
      const resultid = result.split(',');
      getid = resultid [resultid.length - 1];
    } else {
      getid = result;
    }
    console.log(getid);
    this.travel.departure = getid;
    this.travel.departureAddress = selectedData.data.formatted_address;
    const request = {
      origin: this.travel.departureAddress,
      destination: this.travel.destinationAddress,
      travelMode: 'DRIVING'
    };
    if (this.travel.destinationAddress !== '') {
      this.directionsService.route(request, function (response, status) {
        if (status === 'OK') {
          (<HTMLInputElement>document.getElementById('averageTime')).value =
            (response.routes[0].legs[0].distance.value / 1000).toFixed(0);
          (<HTMLInputElement>document.getElementById('distance')).value =
            (response.routes[0].legs[0].duration.value / 60).toFixed(1);
        } else {
          alert('Unable to find the distance via road.');
        }
      });
    }
  }

  autoCompleteCallback2(selectedData: any) {
    this.travelTypes = [];
    console.log(selectedData.data.vicinity);
    console.log(selectedData.data.formatted_address);
    const result = selectedData.data.vicinity;
    const substring = ',';
    let getid;
    if (result.indexOf(substring) >= 0) {
      const resultid = result.split(',');
      getid = resultid [resultid.length - 1];
    } else {
      getid = result;
    }
    console.log(getid);
    this.travel.destination = getid;
    this.travel.destinationAddress = selectedData.data.formatted_address;
    const request = {
      origin: this.travel.departureAddress,
      destination: this.travel.destinationAddress,
      travelMode: 'DRIVING'
    };
    if (this.travel.departureAddress !== '') {
      this.directionsService.route(request, function (response, status) {
        if (status === 'OK') {
          (<HTMLInputElement>document.getElementById('averageTime')).value =
            (response.routes[0].legs[0].distance.value / 1000).toFixed(0);
          (<HTMLInputElement>document.getElementById('distance')).value =
            (response.routes[0].legs[0].duration.value / 60).toFixed(1);
        } else {
          alert('Unable to find the distance via road.');
        }
      });
    }
  }

  addTravel({value, valid}: { value: Travel, valid: boolean }) {

    this.setprice();
    value.date = new Date(this.model.year, this.model.month - 1, this.model.day, this.time.hour, this.time.minute);
    this.travel.averageTime = Number((<HTMLInputElement>document.getElementById('averageTime')).value);
    this.travel.distance = Number((<HTMLInputElement>document.getElementById('distance')).value);
    value.averageTime = Number((<HTMLInputElement>document.getElementById('averageTime')).value);
    value.distance = Number((<HTMLInputElement>document.getElementById('distance')).value);
    if (value.departure == null) {
      value.departure = this.travel.departure.trim();
      value.departureAddress = this.travel.departureAddress;
      value.destination = this.travel.destination.trim();
      value.destinationAddress = this.travel.destinationAddress;
    }
    console.log(value);
    this.modalRef.close();
    if (valid) {
      this.travelService.addTravel(value).subscribe(
        (res) => {
          this.flashMsgService.show('Trajet sauvegardé', {
            cssClass: 'alert alert-success', timeout: 4000
          });

          this.router.navigate(['/travels']);
        },
        (err) => {
          if (err.status === 401) {
            this.flashMsgService.show('Votre compte a peut-être été suspendu, veuillez contacter le support', {
              cssClass: 'alert alert-danger', timeout: 4000
            });
          } else {
            this.flashMsgService.show(err.error.message, {
              cssClass: 'alert alert-danger', timeout: 4000
            });
          }

          this.router.navigate(['/dashboard']);
        });
    }
  }
}
