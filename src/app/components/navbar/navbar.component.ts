import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthService} from '../../users/services/auth.service';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import * as $ from 'jquery';
import {UserService} from '../../users/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NavbarComponent implements OnInit {

  uploadUrl = environment.serverUrl + '/uploads/profile';

  constructor(public authService: AuthService, public userService: UserService, private router: Router) {
    if (this.authService.loggedIn()) {
      this.userService.getMe().subscribe(
        (user) => {
          this.authService.user = user;
          localStorage.setItem('user', JSON.stringify(this.authService.user));
        },
        (err) => {
          console.log(err);
        }
      );
    }
  }

  ngOnInit() {
  }

  logout() {
    this.authService.logout().subscribe(
      (res) => {
        this.router.navigate(['/']);
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          console.log('An error occurred:', err.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
        }
      },
      () => {
        localStorage.removeItem('x-auth');
        localStorage.removeItem('user');
      }
    );
  }

  showNotifications() {
    if ($('#notif-dropdown-menu').hasClass('show')) {
      $('#notifications-count').show();
    } else {
      $('#notifications-count').hide();
    }
  }

  deleteNotification(id) {
    this.userService.deleteNotification(id).subscribe(
      (success) => {
        const index = this.authService.user.notifications.findIndex((notif) => notif._id === id);
        if (index >= 0) {
          this.authService.user.notifications.splice(index, 1);
          localStorage.setItem('user', JSON.stringify(this.authService.user));
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
